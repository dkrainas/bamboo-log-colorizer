!function($) {
	var isErrorLine = function(txt) {
		if(txt.indexOf(': error ') > 0 || txt.indexOf('] Error: ') > 0 || txt.indexOf(' -- FAILED') > 0 || txt.indexOf("] error running") > 0 || txt.indexOf("Failed to execute") > 0 || txt.indexOf("Unable to clean source directory") > -1 || txt.indexOf("Error occurred while executing the build") > -1)
			return true;
			
		return false;
	};
	
	var isWarningLine = function (txt) {
		if(txt.indexOf(': warning ') > 0 || txt.indexOf('] Warning: ') > 0 || txt.indexOf('The system cannot find the file specified.') > 0)
			return true;
			
		return false;
	};

	if ($("body.aui-theme-bamboo #buildLog").size() > 0) {
		$("#buildLog td:last-child").each(function(i, e) {
			if (isWarningLine($(e).text())) {
				$(e).addClass("bamboo-colorizer-warning");
			} else if($(e).hasClass("errorOutputLog") || isErrorLine($(e).text())) {
				$(e).addClass("bamboo-colorizer-error");
			}
			
			if($(e).hasClass("bamboo-colorizer-error") || $(e).hasClass("bamboo-colorizer-warning")) {
				var text = $(e).text();
				$(e).text("");
				$("<div>"+text+"</div>").appendTo(e);
			}
		});
	}
}(window.jQuery);